var Players = [
  {
    name: "Karthik",
    score: 31,
    id: 1
  },
  {
    name: "Danny",
    score: 33,
    id: 2
  },
  {
    name: "Arrow",
    score: 35,
    id: 3
  }
];

function Header(props) {
  return (
    <div className="header">
    <h1>{props.name}</h1>
    </div>
  );
}

Header.propTypes = {
  name: React.PropTypes.string.isRequired
};

var Counter = React.createClass(
  {
    propTypes: {
      score: React.PropTypes.number.isRequired
    },
    render: function() {
      return (
        <div className="counter">
        <button className="counter-action decrement"> - </button>
        <div className="counter-score">{this.props.score}</div>
        <button className="counter-action increment"> + </button>
        </div>
      );
    } /* end of the render*/
  } /* end of the object */
); /*end of the create class*/

function Player(props) {
  return (
    <div className="player">
    <div className="player-name">{props.name}</div>
    <div className="player-score">
    <Counter score={props.score} />
    </div>
    </div>
  );
}

Player.propTypes = {
  name: React.PropTypes.string.isRequired,
  score: React.PropTypes.number.isRequired
};

function Application(props) {
  return (
    <div className="scoreboard">
    <Header name={props.name} />

    <div className="players">
    {props.players.map(function(player) {
    return (
      <Player name={player.name} score={player.score} key={player.id} />
    );
  })}
</div>
</div>
);
}

Application.propTypes = {
  name: React.PropTypes.string,
  players: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      name: React.PropTypes.string.isRequired,
      score: React.PropTypes.number.isRequired,
      id: React.PropTypes.number.isRequired
    })
  ).isRequired
};
Application.defaultprops = {
  name: "ScoreBoard"
};

ReactDOM.render(
  <Application name="ScoreBoard" players={Players} />,
  document.getElementById("container")
);
